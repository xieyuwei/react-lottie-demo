import React, { Component } from "react";
import { findDOMNode } from "react-dom";
import lottie from "lottie-web";

class App extends Component {
  componentDidMount() {
    const elem1 = findDOMNode(this.refs.transloader);
    const elem2 = findDOMNode(this.refs["cyrcadia6.5"]);
    const elem3 = findDOMNode(this.refs["cyrcadia6.8"]);
    const elem4 = findDOMNode(this.refs["cyrcadia6.9"]);
    lottie.loadAnimation({
      container: elem1,
      renderer: "svg",
      loop: true,
      autoplay: true,
      path: "./jsons/transloader.json"
    });
    lottie.loadAnimation({
      container: elem2,
      renderer: "svg",
      loop: true,
      autoplay: true,
      path: "./jsons/6.5/C_motion_6.5-Trial-Run.json"
    });
    lottie.loadAnimation({
      container: elem3,
      renderer: "svg",
      loop: true,
      autoplay: true,
      path: "./jsons/6.8/C_motion_6.8-scanning.json"
    });
    lottie.loadAnimation({
      container: elem4,
      renderer: "svg",
      loop: true,
      autoplay: true,
      path: "./jsons/6.9/C_motion_6.9-Data-Transferring.json"
    });
  }
  render() {
    return (
      <div className="App">
        <div
          id="transloader"
          ref="transloader"
          style={{
            display: "inline-block",
            width: "20%",
            border: "1px solid #000",
            margin: "5px"
          }}
        />
        <div
          id="cyrcadia6.5"
          ref="cyrcadia6.5"
          style={{
            display: "inline-block",
            width: "20%",
            border: "1px solid #000",
            margin: "5px"
          }}
        />
        <div
          id="cyrcadia6.8"
          ref="cyrcadia6.8"
          style={{
            display: "inline-block",
            width: "20%",
            border: "1px solid #000",
            margin: "5px"
          }}
        />
        <div
          id="cyrcadia6.9"
          ref="cyrcadia6.9"
          style={{
            display: "inline-block",
            width: "20%",
            border: "1px solid #000",
            margin: "5px"
          }}
        />
      </div>
    );
  }
}

export default App;
